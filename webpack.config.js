var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/app.js',
    output: {
        path: __dirname + '/dist',
        filename: 'app.budle.js'
    },
    plugins: [new HtmlWebpackPlugin()],
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_module/,
            use: 'babel-loader'
        }]
    }
};