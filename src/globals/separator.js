function separator (title) {
    if (title) {
        console.log(`\n******** ${title} ********\n`);
        return;
    }
    console.log('\n****************\n');
}

export default separator;
