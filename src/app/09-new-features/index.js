const newFeatures = {
    methodDeclaration,
    forOf
};

function methodDeclaration () {
    var myObject = {
        counter: 0,
        increment () {
            return this.counter++;
        }
    };
    console.clear();
    console.log(myObject);
    console.log(myObject.increment(), myObject.increment());
    console.log('method declared wihtout function keyword');
}

function forOf () {
    var myArray = ['one', true, 3];

    console.clear();
    for (let item of myArray) {
        console.log(item);
    }
}



export default newFeatures;
