import separator from '../../globals/separator';

function withArray () {
    console.clear();
    const myArray = ['one', 'two', 'tree', 'four'];

    var [first, second] = myArray;

    console.log('first', first);
    console.log('second', second);

    var [firstAgain, , third] = myArray;

    separator();
    console.log('firstAgain', firstAgain);
    console.log('third', third);
}


function switchFn () {
    var element1 = 'element-1',
        element2 = 'element-2';

    console.clear();

    separator('before');
    console.log('element1', element1);
    console.log('element2', element2);
    [element1, element2] = [element2, element1];
    
    separator('after');
    console.log('element1', element1);
    console.log('element2', element2);
}

function withObjects () {
    var {fName, undefParam} = {
        fName: 'Raffaele',
        lName: 'Mori'
    };

    console.clear();

    console.log('fName', fName);
    console.log('undefParam', undefParam);

    separator('do it complex');
    const originalObj = {a: true, b: 'string'};
    var {a: myBool, b: myStr} = originalObj;

    console.log('myBool', myBool);
    console.log('myStr', myStr);

    separator('obviously');

    var [,secondParam, thirdParam] = returnArray();
    console.log('secondParam', secondParam);
    console.log('thirdParam', thirdParam);

    function returnArray () {
        return [1, 'two', {}];
    }
}

var destructuring = {
    switchFn,
    withArray,
    withObjects
};

export { destructuring };
