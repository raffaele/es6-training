import separator from '../../globals/separator';

function MyClassES5 (p1) {
    this.p1 = p1.toUpperCase();
};

MyClassES5.prototype.method1 = function () {
    return 'method 1 of MyClassES5 class';
}

class MyClassES6 {
    constructor (p1) {
        this.p1 = p1.toUpperCase();
    }
    method1 () {
        return 'method 1 of MyClassES6 class';
    }
    method2 () {
        return 'method 2 of MyClassES6 class';
    }
}

class MySubclassES6 extends MyClassES6 {
    constructor (p1, p2, p3 = true) {
        super(p1);
        this.p2 = p2;
        this.p3 = p3;
        this._innerParam = 0;
    }
    method1 () {
        return 'method 1 of MySubclassES6 class';
    }
}

class Circle {
    constructor (raw) {
        this._raw = raw;
    }

    static formula () {
        return 'R*R*' + Math.PI;
    }

    get area () {
        return this.raw * this.raw * Math.PI;
    }

    get raw () {
        return this._raw;
    }
    set raw (raw) {
        if (raw < 0) {
            throw 'invalid';
        }
        this._raw = raw;
    }
}

function basic () {
    console.clear();
    console.log(new MyClassES5('hello'));
    console.log(new MyClassES6('hi'));
    console.log('type', MyClassES6);
}

function inheritance () {
    var subObject = new MySubclassES6('bye', 'text');
    console.clear();
    console.log(subObject);
    console.log(subObject.method1());
    console.log(subObject.method2());
}

function staticMethods () {
    console.clear();
    console.log(Circle.formula());
}

function gettersAndSetters () {
    var myCircle = new Circle(10);
    console.log('myCircle.area', myCircle.area);
    myCircle.raw = 5;
    console.log('myCircle.area', myCircle.area);
    try {
        myCircle.raw = -10;
    } catch (ex) {
        console.log(ex);
    }
}

const classes = { basic, inheritance, staticMethods, gettersAndSetters };

export default classes;
