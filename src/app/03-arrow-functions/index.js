// import separator from '../../globals/separator';

const initialArr = [1,2,3,4,5,6,7,8,9,10];

function compareFunctions () {
    console.clear();
    var classicDoubleArray = initialArr.map(function (val) {
        return 2 * val;
    });

    var arrowFnDoubleArray = initialArr.map((val) => {
        return 2 * val;
    });

    // If we have just one parameter we can avoid parenthesis
    var simplifiedArrowFnArray = initialArr.map(x => {
        return 2 * x;
    });

    // If we have to return directly a value, without curly brackets
    var verySimplifiedArrowFnArray = initialArr.map(x => 2 * x);

    console.log(classicDoubleArray, 'classicDoubleArray');
    console.log(arrowFnDoubleArray, 'arrowFnDoubleArray');
    console.log(simplifiedArrowFnArray, 'simplifiedArrowFnArray');
    console.log(verySimplifiedArrowFnArray, 'verySimplifiedArrowFnArray');
}

function arrowFunctionReturnObject () {
    console.clear();
    /**
     * Warnings: if we want to return an object we cannot avoid {}:
     * (a) => {param: a}; is wrong (the compiler does not understand if carly brackets are coming for body of function or object return)
     * It should be:
     * (a) => {
     *      return {
     *          param: a
     *      };
     * }
     * 
     * OR
     * 
     * (a) => ( {param: a} ) OR  a => ( {param: a} )
     * 
     */

    var mappedArray = [1,2,3].map( x => ({param: x}) );

    console.log(mappedArray);
}


function arrowNamedFunctions () {
    console.clear();
    console.log('also used as variable');
    var sum = (a, b) => a + b;
    console.log('sum(1, 2)', sum(1, 2));
}

function checkContext () {
    console.clear();
    var that = this;

    setTimeout(function () {
        console.log('is the same context in normal function?', that === this);
    });
    setTimeout(() => {
        console.log('is the same context in arrow function?', that === this);
    });
}

var arrowFunctions = {
    compareFunctions,
    checkContext,
    arrowNamedFunctions
};

export {arrowFunctions};

