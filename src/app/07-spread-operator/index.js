function whatItIs () {
    const simpleArray = [1, 2, 3];

    console.log('Normal array', simpleArray);
    console.log('With spread operator', ...simpleArray);
    console.log('Array elements are reported as they are separated from the array');
}

function toCopyArray () {
    const myArr = [1,2,3,4,5,6,7,8,9,0];
    const myArrCopy = [...myArr];

    console.log(myArr, 'myArr');
    console.log(myArrCopy, 'myArrCopy');
    console.log('Are the same?', myArr === myArrCopy);
}

function toConcatenateElementsIntoArray () {
    var arr1 = ['a', 'b', {}, true],
        arr2 = [1, [], 12.5, null];

    console.log([1, ...arr1, /regExp/, ...arr2]);

    separator('push elements in first or end of array');

    var newElement = 'new',
        newArray = [newElement, ...arr1],
        anotherNewArray = [...arr1, newElement];

    console.log('arr1', arr1);
    console.log('newArray', newArray);
    console.log('anotherNewArray', anotherNewArray);
}

function asMethodParams () {
    function spreadFn (...args) {
        // var args = Array.prototype.slice.apply(arguments);
        return args;
    }

    console.log(spreadFn(1,{},[]));

    function spreadFn2 (a, b, ...otherArgs) {
        return {
            a: a,
            b: b,
            otherArgs: otherArgs
        };
    }
    console.log(spreadFn2('hello', 'world', 'and', 'hello', 'again'));
}

const spreadOperator = {
    whatItIs,
    toCopyArray,
    toConcatenateElementsIntoArray,
    asMethodParams
};

export default spreadOperator;
