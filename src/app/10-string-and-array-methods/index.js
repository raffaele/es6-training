const stringArrayMethods = {
    stringStartsWidth,
    stringEndsWidth,
    stringIncludes,
    arrayFindIndex,
    arrayFill
};

function stringStartsWidth () {
    const myString = 'Hello world';
    console.clear();
    console.log(myString.startsWith('Hello'));
    console.log(myString.startsWith('hello'));
    console.log('In ES6 myString.startsWith(\'Hello\')');
    console.log('In ES5 myString.indexOf(\'Hello\') === 0');
}

function stringEndsWidth () {
    const myString = 'Hello world';
    console.clear();
    console.log(myString.endsWith('world'));
    console.log(myString.endsWith('worl'));
}

function stringIncludes () {
    const myString = 'Hello world';
    console.clear();
    console.log(myString.endsWith('lo wo'));
    console.log('In ES6 myString.endsWith(\'lo wo\')');
    console.log('In ES5 myString.indexOf(\'lo wo\') !== -1');
}

function arrayFindIndex () {
    const myArray = [1, 3, 5, 87];
    console.clear();
    console.log(myArray);
    console.log('myArray.findIndex(x => x===5)', myArray.findIndex(x => x===5));
}

function arrayFill () {
    const myArray = (new Array(5)).fill("hello");
    console.clear();
    console.log('(new Array(5)).fill("hello")', myArray);
    console.log('to sobstutuite an element in array');
    var mySecondArray = myArray.fill('hi', 2, 3);
    console.log(mySecondArray);
}

export default stringArrayMethods;
