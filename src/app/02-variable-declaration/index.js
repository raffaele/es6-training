import separator from '../../globals/separator';

var myVarOldStyle1 = 'old-style';
var myVarOldStyle2;
let myVarNewStyle1 = 'new-style';
let myVarNewStyle2;
const myConst1 = ['I am a constant'];

/** 
 * A constant must be initialised during declaration.
 * If we uncomment the follow line we'll receive js error.
 */ 
// const myConst2;

/**
 * A value of a const cannot be reassigned once initialised
 * If we uncomment the follow line we'll receive js error.
 */
// myConst1 = 'I am another constant';

function updateConstants () {
    console.log('myConst1', myConst1);
    myConst1.push('now updated');
    console.log('myConst1', myConst1);
}

function verifyHoisting () {
    /**
     * Still using hoisting on var
     */
    console.log('typeof anotherConstant', typeof(anotherVar));
    var anotherVar;

    /**
     * Not using hoisting on const/let
     * If we uncomment next 2 lines we'll have a js error
     */
    // console.log('typeof anotherConstant', typeof(anotherConst));
    // console.log('typeof anotherConstant', typeof(anotherLet));
    const anotherConst = 'another constant';
    let anotherLet = 'another let-declared variable';
}

/**
 * We don't need 'use strict' anymore
 * All the undeclared variable will generate automatically js error.
 * If we uncomment this line we'll have a js error
 */
// if (undeclared) {}


function testLet () {
    var myVar = 'myVar';
    let myExternalLet = 'my-let';
    const myExternalConst = 'external-const';
    if (true) {
        const innerConst = 'inner-const';
        const myExternalConst = 'external-inside';
        var myVar = 12;
        let myExternalLet = 'inner-let';
        let myInternalLet = 'just-internal';
        console.log('myExternalLet inside if', myExternalLet);
        if (true) {
            console.log('myInternalLet in nested if', myInternalLet);
        }
    }
    console.log('myVar', myVar);
    console.log('myExternalLet outside if', myExternalLet);
    console.log('my external const value', myExternalConst);
    /**
     * The let/const scope is just the smaller block of {} where it's declared
     * If we uncomment next 2 lines we'll have a js error
     */
    // console.log('type of myInternalLet', myInternalLet);
    // console.log('type of innerConst', innerConst);
};

var declaration = {updateConstants, verifyHoisting, testLet};

export { declaration };
