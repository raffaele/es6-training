function testFn (p1, p2, p3) {
    return {
        p1: p1,
        p2: p2,
        p3: p3
    };
}

const theDefault = {
    type: 'another object'
};

function defaultTestFn (p1, p2 = 'hello', p3 = theDefault) {
    // if (!p2) p2 = 'hello';
    // if (!p3) p3 = theDefault;
    return {
        p1: p1,
        p2: p2,
        p3: p3
    };
}

function defaultParams () {
    console.clear();
    console.log('without default, 3 params', testFn(1, 2, 3));
    console.log('without default, 1 params', testFn(1));
    console.log('with default, 3 params', defaultTestFn(1, 2, 3));
    console.log('with default, 1 params', defaultTestFn(1));
};

export {defaultParams};
