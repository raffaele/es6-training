const nativePromises = {
    basic,
    predefinedBehaviour,
    promiseAll
};

function basic (shouldSucceed) {
    var promise = new Promise((success, fail)=>{
        setTimeout(()=>{
            shouldSucceed ?
                success ('it succeeded') :
                fail ('it failed');
        }, 1000);
    });

    console.clear();
    console.log(`Method called with param ${shouldSucceed}`);

    promise.then(msg => {
        console.log('success', msg);
    }).catch(msg => {
        console.log('reject', msg);
    });
}

function predefinedBehaviour () {
    console.clear();
    Promise.resolve({my:'object'}).then(res => console.log('resolved', res));
    Promise.reject({my:'object'}).catch(res => console.log('rejected', res));
}

function promiseAll () {
    console.clear();
    
    Promise.all([
        Promise.resolve({my:'first'}),
        Promise.resolve({my: 'second'})
    ]).then(res => {
        console.log(res);
    });
    
}

export default nativePromises;
