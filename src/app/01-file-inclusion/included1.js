var myParam = 'my param';
function aFunction () {
    return 'I am the included function';
}

var included = {
    method: aFunction,
    param: myParam
};

export { included };
