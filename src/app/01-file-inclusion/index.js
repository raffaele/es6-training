import {included} from './included1';
import * as lib from './included2';
import myFn from './included3';
import separator from '../../globals/separator';

function fileInclusion () {
    console.clear();
    console.log('We imported the variable from another file, just the module.exported');
    console.log('included value', included);
    
    separator();
    
    console.log('lib array', lib.myArray);
    console.log('lib object', lib.myObject);
    console.log('lib boolean', lib.myBoolean);

    separator();
    
    console.log('myFn', myFn());

    separator();

    console.log('Global variable declared in other files are not visible here');
    console.log('No IIFE needed anymore');
    console.log('typeof myParam', typeof(myParam));
    console.log('typeof aFunction', typeof(aFunction));
}

export {fileInclusion};
