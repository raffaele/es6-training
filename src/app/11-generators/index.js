const generators = {
    basic,
    withObjects,
    advanced
};

function basic () {
    function *generatorFn () {
        yield 1;
        console.log('yield 1 done');
        yield 2;
        console.log('yield 2 done');
        yield 3;
        console.log('yield 3 done');
    }

    var it = generatorFn();
    console.log(it);
    var generatorItem = it.next();
    console.log(generatorItem);

    while(!generatorItem.done) {
        generatorItem = it.next();
        console.log(generatorItem);
    }
    
    console.log(it.next());
}

function withObjects () {
    const myObject = {
        count: 0,
        * getCounter () {
            for (let ii of [1,2,3]) {
                yield this.count++;
            }
        }
    };

    console.log(myObject);
    var iterator = myObject.getCounter();
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
}

function advanced () {
    function* firstGenerator (startValue) {
        var another = yield (startValue + 1);
        console.log(another);
    }
    function *secondGenerator () {
        yield 2;
        yield 4;
        yield 5;
    }

    var sequence = firstGenerator (5);
    for (let item = sequence.next(); !item.done; item = sequence.next()) {
        console.log(item);
    }
    console.log(sequence.next());
}

export default generators;
