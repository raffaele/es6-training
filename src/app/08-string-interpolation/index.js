const strinInterpolation = {
    simpleString,
    interpolation,
    concatenation,
    operations
};

function simpleString () {
    const normalString = 'normal string';
    const multiLineString = `more 
    line
    string`;

    console.clear();
    console.log(normalString);
    console.log(multiLineString);
}

function interpolation () {
    var myValue = 12;
    const notInterpolatedStr = 'not interpolated: ${myValue}';
    const interpolatedStr = `interpolated: ${myValue}`;

    console.clear();
    console.log('notInterpolatedStr', notInterpolatedStr);
    console.log('interpolatedStr', interpolatedStr);
    myValue = 11;
    console.log('If we change the value of the nested variable the string will not be changed', interpolatedStr);
}

function concatenation () {
    const myValue = [1, 2, 3];
    const myString = `Test ${myValue}`;

    console.clear();
    console.log(`myString = '${myString}'`);
}

function operations () {
    const myConst = 10;
    console.clear();
    console.log(`sum result = ${myConst + 10}`);
}

export default strinInterpolation;