import { fileInclusion } from './app/01-file-inclusion/index';
import { declaration } from './app/02-variable-declaration/index';
import { arrowFunctions } from './app/03-arrow-functions/index';
import { defaultParams } from './app/04-default-params/index';
import { destructuring } from './app/05-destructuring/index';
import classes from './app/06-classes/index';
import spreadOperator from './app/07-spread-operator/index';
import stringInterpolation from './app/08-string-interpolation/index';
import newFeatures from './app/09-new-features/index';
import stringArrayMethods from './app/10-string-and-array-methods/index';
import generators from './app/11-generators/index';
import nativePromises from './app/12-native-promises/index';

console.clear();

window.demo = {
    fileInclusion,
    declaration,
    arrowFunctions,
    defaultParams,
    destructuring,
    classes,
    spreadOperator,
    stringInterpolation,
    newFeatures,
    stringArrayMethods,
    generators,
    nativePromises
}
